public class OperadoresDecremento {
   public static void main(String[] args){

    int x = 10;

    x++;
    System.out.println("x = " + x);

    x--;
    System.out.println("x = " + x);

    int y = 10;

    ++y;
    System.out.println("y = " + y);

    --y;
    System.out.println("y = " + y);

   }
}