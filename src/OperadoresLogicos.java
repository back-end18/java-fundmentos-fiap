public class OperadoresLogicos {

    public static void main(String[] args){

        int idade = 20;
        boolean precisaVotar = idade >= 18 && idade < 70; // && verdadeiro

        System.out.println(precisaVotar);

        int nrAmarelo = 2, nrVermelho = 1;
        boolean suspenso = nrAmarelo == 1 || nrVermelho == 0;

        System.out.println(suspenso);

    }   
}