public class HelloJava {

    public static void main(String[] args) {

        String nome = "André Ribeiro";
        int idade = 37;
        String mulher = "Suyane";
        double preco = 11.99;
        int parcela = 12;
        char sexo = 'M';
        boolean temFilhos = true;
        String cidadeNasc = "Nova Aurora";

        System.out.println("Hello Java");
        System.out.println("Seu nome : " + nome);
        System.out.println("Sua idade eh : " + idade);
        System.out.println("Uma linda mulher : " + mulher + " você tem filho " + temFilhos);
        System.out.println("O valor é " + preco );
        System.out.println("O valor do produto é : " + preco * parcela);
        System.out.println("Cidade onde nasceu ? " + cidadeNasc);
        System.out.println("Sexo : " + sexo);

    }
}