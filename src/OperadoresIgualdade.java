public class OperadoresIgualdade {

    public static void main(String[] args) {
        
        int idade  = 20;
        boolean maioridade = idade >= 18;
        boolean maiorIdade = !(idade >= 18); // != Operadores de negação!!


        System.out.println(maioridade);
        System.out.println(maiorIdade);

    }   
}